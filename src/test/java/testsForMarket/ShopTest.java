package testsForMarket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import entities.Item;
import entities.Order;
import entities.User;
import market.Shop;
import market.Status;

public class ShopTest {
    private Map<Item, Long> goods = new HashMap<Item, Long>();
    Item validItem = new Item(1L, "Kettle", 4.99d);

    @BeforeMethod
    public void setUp() {

    }
    
    @AfterMethod
    public void cleanup() {
        goods.clear();
    }

    @Test(priority = 1, dataProvider = "successfully-purchase-data-provider")
    public void shouldSuccessfullyMakePurchase(double userMoney) {
        goods.put(validItem, 1L);
        Shop shop = new Shop(getItemsQuantity());
        User user = new User("John", userMoney);
        Order order = new Order(user, Arrays.asList(validItem));
        Status status = shop.makePurchase(order);

        assert status == Status.OK :
                "Incorrect status after purchase. Expected: " + Status.OK + ", actual: " + status;
    }

    @DataProvider(name = "successfully-purchase-data-provider")
    public Object[][] makePurchaseData() {
        return new Object[][] {{200d},{4.99d}};
    }
    
    @Test(priority = 2)
    public void shouldCorrectlyDecreaseBalance() {
        goods.put(validItem, 2L);
        Shop shop = new Shop(getItemsQuantity());

        double userInitialBalance = 200d;
        User user = new User("John", userInitialBalance);

        List<Item> itemsInOrder = Arrays.asList(validItem, validItem);
        Order order = new Order(user, itemsInOrder);

        double itemsTotalPrice = validItem.getCost() * 2;
        double expectedUserBalance = roundAvoid(userInitialBalance - itemsTotalPrice, 2);

        shop.makePurchase(order);

        assert user.getBalance() == expectedUserBalance :
                "User has insufficient balance. \n Initial user balance: " + userInitialBalance +
                        "\n User current balance: " + user.getBalance() + "\n Items cost: " + itemsTotalPrice;
    }

    @Test(priority = 3)
    public void shouldReturnUnknownItem() {
        goods.clear();
        Shop shop = new Shop(getItemsQuantity());

        User user = new User("John", 400);

        List<Item> itemsInOrder = Arrays.asList(new Item(null, "null", 0));
        Order order = new Order(user, itemsInOrder);

        Assert.assertEquals(shop.makePurchase(order), Status.NON_EXISTING_ITEM,"Incorrect proceed unknown item");
    }

    @Test(priority = 4)
    public void shouldReturnInsufficientItemsStock() {
        goods.clear();
        goods.put(validItem, 1L);
        Shop shop = new Shop(getItemsQuantity());

        User user = new User("John", 4000);

        List<Item> itemsInOrder = Arrays.asList(validItem, validItem);
        Order order = new Order(user, itemsInOrder);

        Assert.assertEquals(shop.makePurchase(order), Status.NOT_ENOUGH_ITEMS_IN_THE_SHOP, "Incorrect calculation in stock");
    }

    @Test(priority = 5)
    public void shouldReturnUserHasLowBalance() {
        goods.clear();
        goods.put(validItem, 1L);
        Shop shop = new Shop(getItemsQuantity());

        double userInitialBalance = validItem.getCost();
        User user = new User("John", userInitialBalance);

        List<Item> itemsInOrder = Arrays.asList(validItem);
        Order order = new Order(user, itemsInOrder);

        assert shop.makePurchase(order) == Status.USER_HAS_LOW_BALANCE : "User balance higher than expected"
                + "\n User current balance: " + user.getBalance();
    }



    
    private Map<Long, Long> getItemsQuantity() {
        return goods.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey().getId(), Map.Entry::getValue));
    }

    private double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
